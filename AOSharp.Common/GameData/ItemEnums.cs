﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Common.GameData
{
    public enum ItemClass
    {
        None = 0,
        Weapon = 1,
        Armor = 2,
        Implant = 3
    }
}
