# Installation Instructions

**Build tutorial**: https://youtu.be/Xx4dYUBavzw  

### 1.  First download these 2 tools.  ###
   - [visual studio 2019 community](https://visualstudio.microsoft.com/vs/community/) - (used to build project and modify code).  
   - [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) - (used to retrieve code from repository).  
  
   <br>
   
### 2.  Create a folder for aosharp and clone repo.  ###
   - Open git bash or command prompt and navigate to location you wish to create folder for aosharp.  
`$ cd <location>`  
   - Create folder for aosharp.  
      ```
      $ mkdir aosharp   
      $ cd aosharp
      ```
   - Clone AOSharp repos into aosharp folder.  
      ```  
      $ git clone https://gitlab.com/never-knows-best/aosharp.git
      $ git clone https://gitlab.com/never-knows-best/aosharp-automation
      $ git clone https://gitlab.com/never-knows-best/aosharp.bots.git
      ```
      
      <br>
      
### 3. Goto folder aosharp/aosharp  ###
   - Double click AOSharp.sln
   - Wait for visual studio to open, **wait 2m.**
   - Press ctrl shift + b
   - Close visual studio.

   <br>

### 4. Goto folder aosharp/aosharp.bots  ###
   - Double click AOSharp.Bots.sln
   - Wait for visual studio to open, **wait 2m.**
   - Press ctrl shift + b.
   - Close visual studio. 

   <br>
   
### 5. Goto folder aosharp/aosharp-automation.  ###
   - Double click AOSharp Automation.sln
   - Wait for visual studio to open, **wait 2m**
   - Press ctrl shift + b
   - Close visual studio.

<br>

### 6. When built goto aosharp/AOSharp/bin/Debug  ###
   - run AOSharp.exe
   - Add plugins, enjoy:)

<br>

**FAQ**: you may need to install dotNET dependencies. visual studio will give u link to download and install it.

<br>

# Plugins
  - [AOSharp Bots](https://gitlab.com/never-knows-best/aosharp.bots)  
  - [AOSharp Automation](https://gitlab.com/never-knows-best/aosharp-automation)

<br>

# Social Links
  - [Discord](https://discord.gg/UyVD7C9) - for any questions/requests.
